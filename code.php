<?php 
	$products = [];

	function createProduct(&$products, $productName, $productPrice) {

		array_push($products, [
			"name" => $productName,
			"price" => $productPrice
		]);
	}
	
	function printProducts(&$products) {

		foreach($products as $product){
			echo "<ul>";
			foreach($product as $key => $value){
				echo "<li> $key : $value </li>";
			}
			echo "</ul>";
		}
	}

	function countProducts(&$products){
		// global $products;

		echo count($products);
	}

	function deleteProduct(&$products){
		array_pop($products);
	}

?>