<?php require './code.php'; ?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Array Manipulation Activity</title>
</head>
<body>
	<h1>Activity</h1>

	<pre>
		<?php
			createProduct($products, 'humankind', 200);
			createProduct($products, 'dota2', 1200);
		?>
	</pre>	

	<pre>
		List of products
		<?php
			printProducts($products);
		?>
	</pre>

	<pre>
		Product count
		<?php
			countProducts($products);
		?>
	</pre>

	<pre>
		Delete Product
		<?php
			deleteProduct($products);
			printProducts($products);
		?>
	</pre>
	
</body>
</html>